<?php

namespace WebartDesign\CustomNotification\Transformers;

use League\Fractal\TransformerAbstract;
use WebartDesign\CustomNotification\Models\CustomNotification;

class CustomNotificationTransformer extends TransformerAbstract
{
    public function transform(CustomNotification $customNotification)
    {
        return array_merge(
            $customNotification->attributesToArray(),
            [
                'recipient_count' => $customNotification->recipientCount()
            ]
        );
    }
}
