<?php

namespace WebartDesign\CustomNotification\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;
use WebartDesign\CustomNotification\Models\CustomNotification;
use WebartDesign\CustomNotification\Models\CustomNotificationRecipient;

class SendCustomNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var CustomNotification
     */
    private $custom_notification;

    /**
     * Create a new job instance.
     * @param CustomNotification $custom_notification
     */
    public function __construct(CustomNotification $custom_notification)
    {
        $this->custom_notification = $custom_notification;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $notification = new \WebartDesign\CustomNotification\Notifications\CustomNotification($this->custom_notification);
        $this->custom_notification->recipients()->chunk(100, function (Collection $recipients) use ($notification) {
            foreach ($recipients as $recipient) {
                /**
                 * @var CustomNotificationRecipient $recipient
                 */
                Notification::send(config('custom-notifications.user_class')::find($recipient->user_id), $notification);
            }
        });
    }
}