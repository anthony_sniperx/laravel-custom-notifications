<?php

namespace WebartDesign\CustomNotification\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Validator;
use WebartDesign\CustomNotification\Models\CustomNotification;

class StoreCustomNotificationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|string|max:50',
            'body' => 'required|string|max:150',
        ];
    }

    /**
     * @return string
     */
    public function title()
    {
        return $this->get('title');
    }

    /**
     * @return string
     */
    public function body()
    {
        return $this->get('body');
    }

    /**
     * @return int
     */
    public function totalLength()
    {
        return strlen($this->title() . $this->body());
    }

    /**
     * @param Validator $validator
     */
    public function withValidator(Validator $validator)
    {
        $validator->after(function (Validator $validator) {
            if ($validator->errors()->any()) {
                return;
            }
            if ($this->totalLength() > CustomNotification::MAX_LENGTH) {
                $validator->errors()->add('title', 'The title and body must have a total length not greater than 200 characters.');
            }
        });
    }
}
