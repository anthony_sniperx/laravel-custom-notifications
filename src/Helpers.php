<?php

namespace WebartDesign\CustomNotification;

class Helpers
{
    public static function setDbConnectionToUtf8mb4()
    {
        config([
            'database.default' => 'mysql_utf8mb4'
        ]);
    }

    public static function setDbConnectionToDefault()
    {
        config([
            'database.default' => env('DB_CONNECTION', 'mysql')
        ]);
    }
}
