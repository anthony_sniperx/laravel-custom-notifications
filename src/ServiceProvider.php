<?php

namespace WebartDesign\CustomNotification;

class ServiceProvider extends \Illuminate\Support\ServiceProvider
{
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/custom-notifications.php' => config_path('custom-notifications.php')
        ]);
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');
    }

    public function register()
    {

    }
}
