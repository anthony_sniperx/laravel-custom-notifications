<?php

namespace WebartDesign\CustomNotification\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string body
 * @property string title
 */
class CustomNotification extends Model
{
    protected $guarded = [];
    const MAX_LENGTH = 200;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function recipients()
    {
        return $this->hasMany(CustomNotificationRecipient::class);
    }

    /**
     * @return int
     */
    public function recipientCount()
    {
        return $this->recipients()->count();
    }
}
