<?php

namespace WebartDesign\CustomNotification;

use Illuminate\Foundation\Auth\User;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use League\Fractal\Manager;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use League\Fractal\Serializer\ArraySerializer;
use WebartDesign\CustomNotification\Jobs\SendCustomNotificationJob;
use WebartDesign\CustomNotification\Models\CustomNotification;
use WebartDesign\CustomNotification\Requests\StoreCustomNotificationRequest;
use WebartDesign\CustomNotification\Transformers\CustomNotificationTransformer;

class CustomNotificationController extends Controller
{
    use ValidatesRequests;

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $this->validate($request, [
            'per_page' => 'nullable|integer',
            'page' => 'nullable|integer'
        ]);
        Helpers::setDbConnectionToUtf8mb4();
        $query = CustomNotification::query();
        $pagination = $query->paginate($request->get('per_page', 20));
        $notifications = $pagination->items();
        return (new Manager())
            ->parseIncludes($request->get('include', ''))
            ->setSerializer(new ArraySerializer())
            ->createData(
                (new Collection($notifications, new CustomNotificationTransformer()))->setPaginator(new IlluminatePaginatorAdapter($pagination))
            )
            ->toArray();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCustomNotificationRequest $request
     * @return array
     */
    public function store(StoreCustomNotificationRequest $request)
    {
        $this->validate($request, [
            'title' => 'required|string|max:50',
            'body' => 'required|string|max:150',
        ]);
        Helpers::setDbConnectionToUtf8mb4();
        $custom_notification = null;
        DB::transaction(function () use ($request, &$custom_notification) {
            /**
             * @var CustomNotification $custom_notification
             */
            $custom_notification = CustomNotification::query()->create([
                'title' => $request->title(),
                'body' => $request->body(),
            ]);
            $class = config('custom-notifications.user_class');
            $user_ids = $class::query()
                ->select('id')
                ->pluck('id');
            foreach ($user_ids as $user_id) {
                $custom_notification->recipients()->create([
                    'user_id' => $user_id
                ]);
            }
        });
        Helpers::setDbConnectionToDefault();
        dispatch(new SendCustomNotificationJob($custom_notification));
        return (new Manager())
            ->setSerializer(new ArraySerializer())
            ->parseIncludes($request->get('include', ''))
            ->createData(new Item($custom_notification, new CustomNotificationTransformer()))
            ->toArray();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return void
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return void
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return void
     */
    public function destroy($id)
    {
        //
    }
}
