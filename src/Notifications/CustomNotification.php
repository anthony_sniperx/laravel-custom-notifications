<?php

namespace WebartDesign\CustomNotification\Notifications;

use WebartDesign\CustomNotification\Helpers;
use WebartDesign\CustomNotification\Models\CustomNotification as CustomNotificationModel;
use WebartDesign\SnsPush\SnsPushChannel;
use WebartDesign\SnsPush\PushNotificationBuilder;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use WebartDesign\SnsPush\SnsPushNotification;

class CustomNotification extends Notification implements ShouldQueue, SnsPushNotification
{
    use Queueable;
    /**
     * @var CustomNotificationModel
     */
    public $custom_notification_model;

    /**
     * Create a new notification instance.
     * @param CustomNotificationModel $custom_notification
     */
    public function __construct(CustomNotificationModel $custom_notification)
    {
        $this->onQueue('mail');
        Helpers::setDbConnectionToUtf8mb4();
        $this->custom_notification_model = $custom_notification;
        Helpers::setDbConnectionToDefault();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SnsPushChannel::class, 'database'];
    }

    /**
     * @param $notifiable
     * @return PushNotificationBuilder
     */
    public function toSnsPush($notifiable)
    {
        return (new PushNotificationBuilder())
            ->setTitle($this->custom_notification_model->title)
            ->setBody($this->custom_notification_model->body);
    }

    /**
     * @param $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'title' => $this->custom_notification_model->title,
            'body' => $this->custom_notification_model->body
        ];
    }
}
