<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomNotificationRecipientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custom_notification_recipients', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('custom_notification_id')->unsigned();
            $table->foreign('custom_notification_id')->references('id')->on('custom_notifications')->onDelete('cascade');
            $table->string('user_id')->index();
            $table->dateTime('sent_at')->nullable();
            $table->dateTime('failed_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custom_notification_recipients');
    }
}
